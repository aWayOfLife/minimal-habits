import type { Handle } from '@sveltejs/kit';
import { VITE_API_PROXY_PATH, API_BASE_URL } from '$env/static/private';

const handleApiProxy: Handle = async ({ event }) => {

	const strippedPath = event.url.pathname.substring(VITE_API_PROXY_PATH.length);

	const urlPath = `${API_BASE_URL}${strippedPath}${event.url.search}`;
	const proxiedUrl = new URL(urlPath);

	event.request.headers.delete('connection');

	return fetch(proxiedUrl.toString(), {
		body: event.request.body,
		method: event.request.method,
		headers: event.request.headers
	}).catch((err) => {
		console.log('Could not proxy API request: ', err);
		throw err;
	});
};

export const handle: Handle = async ({ event, resolve }) => {
	if (event.url.pathname.startsWith(VITE_API_PROXY_PATH)) {
		return await handleApiProxy({ event, resolve });
	}
	return await resolve(event);
};
