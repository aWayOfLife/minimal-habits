export const habitCheckinStartEvent = 'habitCheckinStartEvent';
export const habitCheckinEndEvent = 'habitCheckinEndEvent';

export const fabClickEvent = 'fabClickEvent';
export const dialogCloseEvent = 'dialogCloseEvent';

export const commitmentChangeEvent = 'commitmentChangeEvent';

export const linkAccountTypeSwitchEvent = 'linkAccountTypeSwitchEvent';

export const calendarMonthChangeEvent = 'calendarMonthChangeEvent';
