export const habitStoreKey = 'habitStore';
export const checkinStoreKey = 'checkinStore';
export const habitCheckinStoreKey = 'habitCheckinStore';
export const statisticsStoreKey = 'statisticsStore';
export const accountStoreKey = 'accountStore';
export const userStoreKey = 'userStore';
