export interface IAccount {
	accountId: string;
	name: string;
	email: string;
	password?: string;
}
