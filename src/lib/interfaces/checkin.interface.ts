export interface ICheckin {
	habitId: string;
	checkinId?: string;
	content: string;
	checkInDateTime: Date;
}

export interface ICheckinDay extends ICheckin {
	date: Date;
	isCheckin: boolean;
}
