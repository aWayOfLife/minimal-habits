import type { ICheckin } from './checkin.interface';

export interface IHabit {
	habitId: string;
	title: string;
	commitment: number;
	checkins?: ICheckin[];
}

export interface IStatHabit extends IHabit {
	checkinsThisWeek: number;
}

export interface IHabitCheckin extends IHabit {
	checkins: ICheckin[];
}
