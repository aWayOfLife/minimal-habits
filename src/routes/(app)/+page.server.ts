import type { IHabit } from '$lib/interfaces/habit.interface.js';
import type { ICheckin } from '$lib/interfaces/checkin.interface';
import { parseISO } from 'date-fns';

export async function load({ fetch }) {
	const resHabits = await fetch(`/api/habit`);
	const habits = (await resHabits.json()) as IHabit[];
	let  checkins: ICheckin[] = [];
	habits?.forEach((habit) => {
		checkins.push(...(habit.checkins ?? []));
	});

	checkins = checkins.map((checkin) => ({
		...checkin,
		checkInDateTime: parseISO(checkin.checkInDateTime as unknown as string)
	}));

	return { habits, checkins };
}
