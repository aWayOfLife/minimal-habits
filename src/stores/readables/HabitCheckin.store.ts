import { derived, type Readable } from 'svelte/store';
import { endOfMonth, endOfWeek, startOfWeek } from 'date-fns';
import type { IHabit, IHabitCheckin } from '$lib/interfaces/habit.interface';
import type { ICheckinStore } from '../writables/Checkin.store';
import type { IHabitStore } from '../writables/Habit.store';

export const createHabitCheckinStore =
	() =>
	(habitStore: IHabitStore, checkinStore: ICheckinStore): Readable<IHabitCheckinStore> =>
		derived([habitStore, checkinStore], ([habits, checkins]) => {
			const getHabitCheckins = (): IHabitCheckin[] => {
				return habits.map((habit) => ({
					...habit,
					checkins: checkins.filter((checkin) => checkin.habitId === habit.habitId)
				}));
			};

			const getHabitCheckinsForCurrentWeek = (): IHabitCheckin[] => {
				const currentDate = new Date();
				const firstDayOfWeek = startOfWeek(currentDate, { weekStartsOn: 0 });
				const lastDayOfWeek = endOfWeek(currentDate, { weekStartsOn: 0 });

				return habits.map((habit) => ({
					...habit,
					checkins: checkins.filter(
						(checkin) =>
							checkin.habitId === habit.habitId &&
							checkin.checkInDateTime >= firstDayOfWeek &&
							checkin.checkInDateTime <= lastDayOfWeek
					)
				}));
			};

			const getHabitCheckinsByHabit = (habitId: string): IHabitCheckin => {
				const habit = habits.find((habit) => habit.habitId === habitId) as IHabit;
				const checkinsForHabit = checkins.filter((checkin) => checkin.habitId === habitId);
				return {
					...habit,
					checkins: checkinsForHabit
				};
			};

			const getHabitCheckinsByHabitAndMonth = (
				habitId: string,
				firstDayOfMonth: Date
			): IHabitCheckin => {
				const lastDayOfMonth = endOfMonth(firstDayOfMonth);

				const habit = habits.find((habit) => habit.habitId === habitId) as IHabit;
				const checkinsForHabit = checkins.filter(
					(checkin) =>
						checkin.habitId === habit.habitId &&
						checkin.checkInDateTime >= firstDayOfMonth &&
						checkin.checkInDateTime <= lastDayOfMonth
				);
				return {
					...habit,
					checkins: checkinsForHabit
				};
			};

			return {
				getHabitCheckins: () => getHabitCheckins(),
				getHabitCheckinsForCurrentWeek: () => getHabitCheckinsForCurrentWeek(),
				getHabitCheckinsByHabit: (habitId: string) => getHabitCheckinsByHabit(habitId),
				getHabitCheckinsByHabitAndMonth: (habitId: string, firstDayOfMonth: Date) =>
					getHabitCheckinsByHabitAndMonth(habitId, firstDayOfMonth)
			};
		});

export interface IHabitCheckinStore {
	getHabitCheckins: () => IHabitCheckin[];
	getHabitCheckinsForCurrentWeek: () => IHabitCheckin[];
	getHabitCheckinsByHabit: (habitId: string) => IHabitCheckin;
	getHabitCheckinsByHabitAndMonth: (habitId: string, firstDayOfMonth: Date) => IHabitCheckin;
}
