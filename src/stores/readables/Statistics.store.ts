import { derived, type Readable } from 'svelte/store';
import { endOfWeek, startOfWeek } from 'date-fns';
import type { IHabitCheckin, IStatHabit } from '$lib/interfaces/habit.interface';
import type { IHabitCheckinStore } from './HabitCheckin.store';

const getCurrentWeekDates = () => {
	const currentDate = new Date();
	const firstDayOfWeek = startOfWeek(currentDate, { weekStartsOn: 0 });
	const lastDayOfWeek = endOfWeek(currentDate, { weekStartsOn: 0 });

	return { firstDayOfWeek, lastDayOfWeek };
};

export const createStatisticsStore =
	() =>
	(habitCheckinStore: Readable<IHabitCheckinStore>): Readable<IStatisticsStore> =>
		derived(habitCheckinStore, (habitCheckins) => {
			const { firstDayOfWeek, lastDayOfWeek } = getCurrentWeekDates();

			const getHabitsThisWeek = () =>
				habitCheckins
					.getHabitCheckins()
					.filter((habit) =>
						habit.checkins.some(
							(checkin) =>
								checkin.checkInDateTime >= firstDayOfWeek &&
								checkin.checkInDateTime <= lastDayOfWeek
						)
					);

			const getTotalCheckInsThisWeek = () =>
				getHabitsThisWeek().reduce(
					(total, habit) =>
						total +
						habit.checkins.filter(
							(checkin) =>
								checkin.checkInDateTime >= firstDayOfWeek &&
								checkin.checkInDateTime <= lastDayOfWeek
						).length,
					0
				);

			const getTotalCommitmentThisWeek = () =>
				habitCheckins.getHabitCheckins().reduce((total, habit) => total + habit.commitment, 0);

			const getUniqueDaysCheckedIn = () => {
				const uniqueDaysCheckedIn = new Set();
				getHabitsThisWeek().forEach((habit) => {
					habit.checkins.forEach((checkin) => {
						if (
							checkin.checkInDateTime >= firstDayOfWeek &&
							checkin.checkInDateTime <= lastDayOfWeek
						) {
							uniqueDaysCheckedIn.add((checkin.checkInDateTime ?? new Date()).toDateString());
						}
					});
				});
				const numberOfUniqueDaysCheckedIn = uniqueDaysCheckedIn.size;
				return numberOfUniqueDaysCheckedIn;
			};

			const processHabitsForStatistics = (): {
				mostCheckedInHabits: IStatHabit[];
				leastCheckedInHabits: IStatHabit[];
			} => {
				const mostCheckedInHabits: IStatHabit[] = [];
				const leastCheckedInHabits: IStatHabit[] = [];
				let maxCheckIns = 0;
				let minCheckIns = Number.MAX_VALUE;

				getHabitsThisWeek().forEach((habit) => {
					const checkInsCount = habit.checkins.filter(
						(checkin) =>
							checkin.checkInDateTime >= firstDayOfWeek && checkin.checkInDateTime <= lastDayOfWeek
					).length;

					const adjustedCheckIns =
						checkInsCount * (habit.commitment / getTotalCommitmentThisWeek());

					if (adjustedCheckIns > maxCheckIns) {
						mostCheckedInHabits.length = 0;
						mostCheckedInHabits.push({ ...habit, checkinsThisWeek: checkInsCount });
						maxCheckIns = adjustedCheckIns;
					} else if (adjustedCheckIns === maxCheckIns) {
						mostCheckedInHabits.push({ ...habit, checkinsThisWeek: checkInsCount });
					}

					if (adjustedCheckIns < minCheckIns) {
						leastCheckedInHabits.length = 0; // Reset the array for a new lowest count
						leastCheckedInHabits.push({ ...habit, checkinsThisWeek: checkInsCount });
						minCheckIns = adjustedCheckIns;
					} else if (adjustedCheckIns === minCheckIns) {
						leastCheckedInHabits.push({ ...habit, checkinsThisWeek: checkInsCount });
					}
				});

				// Check for habits with zero check-ins and highest commitment
				const habitsWithZeroCheckIns = habitCheckins
					.getHabitCheckins()
					.filter((habit) =>
						habit.checkins.every(
							(checkin) =>
								checkin.checkInDateTime < firstDayOfWeek || checkin.checkInDateTime > lastDayOfWeek
						)
					);

				if (habitsWithZeroCheckIns.length > 0) {
					const highestCommitment = habitsWithZeroCheckIns.reduce(
						(maxCommitment, habit) => Math.max(maxCommitment, habit.commitment),
						0
					);

					const habitsWithHighestCommitment = habitsWithZeroCheckIns.filter(
						(habit) => habit.commitment === highestCommitment
					);

					leastCheckedInHabits.length = 0; // Reset the array
					leastCheckedInHabits.push(
						...habitsWithHighestCommitment.map((habit) => ({ ...habit, checkinsThisWeek: 0 }))
					);
				}

				return { mostCheckedInHabits, leastCheckedInHabits };
			};

			const getMostCheckedInHabits = (): IStatHabit[] => {
				const { mostCheckedInHabits } = processHabitsForStatistics();
				return mostCheckedInHabits;
			};

			const getLeastCheckedInHabits = (): IStatHabit[] => {
				const { leastCheckedInHabits } = processHabitsForStatistics();
				return leastCheckedInHabits;
			};

			return {
				getHabitsThisWeek,
				getTotalCheckInsThisWeek,
				getTotalCommitmentThisWeek,
				getUniqueDaysCheckedIn,
				getMostCheckedInHabits,
				getLeastCheckedInHabits
			};
		});

export interface IStatisticsStore {
	getHabitsThisWeek: () => IHabitCheckin[];
	getTotalCheckInsThisWeek: () => number;
	getTotalCommitmentThisWeek: () => number;
	getUniqueDaysCheckedIn: () => number;
	getMostCheckedInHabits: () => IStatHabit[];
	getLeastCheckedInHabits: () => IStatHabit[];
}
