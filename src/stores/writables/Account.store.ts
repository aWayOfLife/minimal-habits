import type { IAccount } from '$lib/interfaces/account.interface';
import { writable, type Writable } from 'svelte/store';

export const createAccountStore = () => (): IAccountStore => {
	const { subscribe, set } = writable<IAccount | null>();

	return {
		subscribe,
		set
	};
};

export interface IAccountStore {
	subscribe: Writable<IAccount | null>['subscribe'];
	set: (this: void, value: IAccount | null) => void;
}
