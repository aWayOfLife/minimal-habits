import { writable, type Writable } from 'svelte/store';
import { isSameDay } from 'date-fns';
import type { ICheckin } from '$lib/interfaces/checkin.interface';

export const createCheckinStore = () => (): ICheckinStore => {
	const { subscribe, update, set } = writable<ICheckin[]>([]);
	const addCheckin = (checkin: ICheckin) => {
		checkin = { ...checkin, checkInDateTime: new Date() };
		update((existingCheckins) => {
			const existingCheckinForHabitOnSameDay = existingCheckins.find(
				(existingCheckin) =>
					existingCheckin.habitId === checkin.habitId &&
					isSameDay(existingCheckin.checkInDateTime, checkin.checkInDateTime)
			);

			if (existingCheckinForHabitOnSameDay) {
				existingCheckinForHabitOnSameDay.checkInDateTime = checkin.checkInDateTime;
				existingCheckinForHabitOnSameDay.content = checkin.content;
			} else {
				checkin = { ...checkin };
				existingCheckins = [checkin, ...existingCheckins];
			}
			return existingCheckins;
		});
	};

	return {
		subscribe,
		set,
		addCheckin: async (checkin: ICheckin) => await addCheckin(checkin)
	};
};

export interface ICheckinStore {
	subscribe: Writable<ICheckin[]>['subscribe'];
	set: (this: void, value: ICheckin[]) => void;
	addCheckin: (checkin: ICheckin) => void;
}
