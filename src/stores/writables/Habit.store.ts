import type { IHabit } from '$lib/interfaces/habit.interface';
import { writable, type Writable } from 'svelte/store';

export const createHabitStore = () => (): IHabitStore => {
	const { subscribe, set, update } = writable<IHabit[]>([]);

	const addHabit = async (habit: Partial<IHabit>) => {
		const newHabit = { ...habit } as IHabit;
		update((existingHabits) => {
			return [...existingHabits, newHabit];
		});
	};

	const removeHabit = async (habitId: string) => {
		update((existingHabits) => {
			const filteredHabits = existingHabits.filter(
				(existingHabit) => existingHabit.habitId !== habitId
			);
			return [...filteredHabits];
		});
	};

	return {
		subscribe,
		set,
		addHabit: async (habit: Partial<IHabit>) => await addHabit(habit),
		removeHabit: async (habitId: string) => await removeHabit(habitId)
	};
};

export interface IHabitStore {
	subscribe: Writable<IHabit[]>['subscribe'];
	set: (this: void, value: IHabit[]) => void;
	addHabit: (habit: Partial<IHabit>) => Promise<void>;
	removeHabit: (habitId: string) => Promise<void>;
}
