import { auth } from '$lib/firebase/firebase.client';
import type { IAccount } from '$lib/interfaces/account.interface';
import {
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	signOut,
	updateProfile,
	type User
} from 'firebase/auth';
import { writable, type Writable } from 'svelte/store';

export const createUserStore = () => (): IUserStore => {
	const { subscribe, set } = writable<User | null>();

	const createUser = async (account: Partial<IAccount>) => {
		await createUserWithEmailAndPassword(auth, account.email as string, account.password as string);
		const currentUser = auth.currentUser;
		if (currentUser) {
			await updateProfile(currentUser, { displayName: account.name });
			set(currentUser);
		}
	};

	const signInUser = async (account: Partial<IAccount>) => {
		await signInWithEmailAndPassword(auth, account.email as string, account.password as string);
	};

	const signOutUser = async () => {
		await signOut(auth);
	};

	return {
		subscribe,
		set,
		createUser: async (account: Partial<IAccount>) => await createUser(account),
		signInUser: async (account: Partial<IAccount>) => await signInUser(account),
		signOutUser: async () => await signOutUser()
	};
};

export interface IUserStore {
	subscribe: Writable<User | null>['subscribe'];
	set: (this: void, value: User | null) => void;
	createUser: (account: Partial<IAccount>) => void;
	signInUser: (account: Partial<IAccount>) => void;
	signOutUser: () => void;
}
