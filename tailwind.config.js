/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				'mh-gray-90': '#08080A',
				'mg-gray-70': '#7D7E84',
				'mh-gray-50': '#BEC0C7',
				'mh-gray-30': '#DADBDF',
				'mh-gray-20': '#E8E8EB',
				'mh-gray-10': '#F6F6F7'
			},
			fontFamily: {
				sans: ['Outfit', 'system-ui'],
				serif: ['Outfit', 'Georgia'],
				mono: ['Outfit', 'SFMono-Regular'],
				display: ['Outfit'],
				body: ['"Outfit"']
			}
		}
	},
	plugins: []
};
